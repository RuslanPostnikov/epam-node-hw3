const {Schema, model} = require('mongoose');

const truckModel = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

module.exports = model('Truck', truckModel);
