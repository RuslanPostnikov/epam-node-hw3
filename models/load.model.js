const {Schema, model} = require('mongoose');

const loadModel = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  assigned_to: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    required: true,
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: {width: Number, length: Number, height: Number},
    required: true,
    _id: false,
  },
  logs: {
    type: [{message: String, time: String}],
    default: undefined,
    _id: false,
  },
  created_date: {
    type: String,
    required: true,
  },
});

module.exports = model('Load', loadModel);
