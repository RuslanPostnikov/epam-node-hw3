const statusArray = ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'];

const stateArray = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery'];

module.exports = {statusArray, stateArray};

