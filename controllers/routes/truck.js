const {Router} = require('express');
const logger = require('../../logger');
const Truck = require('../../models/truck.model');
const {verifyToken, isDriver} = require('../middleware/authJwt');
const {validateTruckType} = require('../middleware/validation');

const router = Router();

router.post('/api/trucks', verifyToken, isDriver, async (req, res, next) => {
  try {
    const truck = new Truck({
      created_by: req.userId,
      type: req.body.type,
      status: 'IS',
      createdDate: new Date().toLocaleString(),
    });

    await truck.save();

    logger.info('Truck created successfully');

    res.type('application/json').json({message: 'Truck created successfully'});
  } catch (e) {
    next(e);
  }
});

router.get('/api/trucks', verifyToken, isDriver, async (req, res, next) => {
  try {
    const trucks = await Truck.find({created_by: req.userId}).select('-__v');

    if (!trucks) return res.status(400).json({message: `No trucks in database`});

    logger.info(trucks);

    res.type('application/json').json({trucks});
  } catch (e) {
    next(e);
  }
});

router.get('/api/trucks/:id', verifyToken, isDriver, async (req, res, next) => {
  try {
    const truck = await Truck.findOne({_id: req.params.id, userId: req.userId}).select('-__v');

    if (!truck) return res.status(400).json({message: `No truck with requested id ${req.params.id}`});

    logger.info(truck);

    res.type('application/json').json({truck});
  } catch (e) {
    next(e);
  }
});

router.put('/api/trucks/:id', verifyToken, isDriver, validateTruckType, async (req, res, next) => {
  try {
    const truck = await Truck.findOneAndUpdate(
        {_id: req.params.id, userId: req.userId},
        {type: req.body.type},
    );

    if (!truck) return res.status(400).json({message: `No truck with requested id ${req.params.id}`});

    logger.info({message: 'Truck details changed successfully'});

    res.type('application/json').json({message: 'Truck details changed successfully'});
  } catch (e) {
    next(e);
  }
});

router.delete('/api/trucks/:id', verifyToken, isDriver, async (req, res, next) => {
  try {
    const truck = await Truck.findOneAndDelete();

    if (!truck) return res.status(400).json({message: `No truck with requested id ${req.params.id}`});

    logger.info({message: 'Truck deleted successfully'});

    res.type('application/json').json({message: 'Truck deleted successfully'});
  } catch (e) {
    next(e);
  }
});

router.post('/api/trucks/:id/assign', verifyToken, isDriver, async (req, res, next) => {
  try {
    console.log(req.params.id);
    // if (!req.params.id) return res.status(200).json({message: 'Please specify truck id'});
    const assignedTruck = await Truck.findOne({assigned_to: req.userId});
    if (assignedTruck) return res.status(400).json({message: 'You already have assigned truck'});
    const truck = await Truck.findOneAndUpdate(
        {_id: req.params.id, userId: req.userId},
        {assigned_to: req.userId},
    );

    if (!truck) return res.status(400).json({message: `No truck with requested id ${req.params.id}`});

    logger.info({message: 'Truck assigned successfully'});

    res.type('application/json').json({message: 'Truck assigned successfully'});
  } catch (e) {
    next(e);
  }
});

router.post('/api/trucks//assign', verifyToken, isDriver, async (req, res, next) => {
  try {
    return res.status(200).json({message: 'Please specify truck id'});
  } catch (e) {
    next(e);
  }
});

module.exports = router;
