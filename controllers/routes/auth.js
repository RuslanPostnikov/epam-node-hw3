const {Router} = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const router = Router();
const User = require('../../models/user.model');
const verifySignUp = require('../middleware/verifySignUp');
const logger = require('../../logger');

router.post('/api/auth/register', verifySignUp, async (req, res, next) => {
  try {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept',
    );

    const user = new User({
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      role: req.body.role,
      createdDate: new Date().toLocaleString(),
    });
    await user.save();

    logger.info(user);

    res.type('application/json').json({message: 'Profile created successfully'});
  } catch (e) {
    next(e);
  }
});

router.post('/api/auth/login', async (req, res, next) => {
  try {
    res.header(
        'Access-Control-Allow-Headers',
        'x-access-token, Origin, Content-Type, Accept',
    );

    User.findOne({
      email: req.body.email,
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({message: err});
        return;
      }
      if (!user) {
        return res.status(404).send({message: 'User Not found.'});
      }
      const passwordIsValid = bcrypt.compareSync(
          req.body.password,
          user.password,
      );
      if (!passwordIsValid) {
        return res.status(400).send({
          message: 'Invalid Password!',
        });
      }
      const token = jwt.sign({id: user.id}, process.env.SECRET, {
        expiresIn: 86400, // 24 hours
      });

      res.status(200).send({
        jwt_token: token,
      });
    });
  } catch (e) {
    next(e);
  }
});

module.exports = router;
