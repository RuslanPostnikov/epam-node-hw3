const {Router} = require('express');
const logger = require('../../logger');
const Load = require('../../models/load.model');
const Truck = require('../../models/truck.model');
const {verifyToken, isShipper, checkRole, isDriver} = require('../middleware/authJwt');

const router = Router();

router.get('/api/loads', verifyToken, checkRole, async (req, res, next) => {
  try {
    const filterField = req.userRole === 'SHIPPER' ? 'created_by' : 'assigned_to';
    const loads = await Load
        .find({[filterField]: req.userId})
        .select('-__v');

    if (!loads) return res.status(400).json({message: `No loads in database`});

    logger.info(loads);

    res.type('application/json').json({loads});
  } catch (e) {
    next(e);
  }
});

router.post('/api/loads', verifyToken, isShipper, async (req, res, next) => {
  try {
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
    const load = new Load({
      created_by: req.userId,
      status: 'NEW',
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      created_date: new Date().toLocaleString(),
    });

    await load.save();

    logger.info('Load created successfully');

    res.type('application/json').json({message: 'Load created successfully'});
  } catch (e) {
    next(e);
  }
});

router.get('/api/loads/active', verifyToken, isDriver, async (req, res, next) => {
  try {
    const load = await Load
        .findOne({assigned_to: req.userId})
        .select('-__v');

    if (!load) return res.status(200).json({message: `No load in database`});

    logger.info(load);

    res.type('application/json').json({load});
  } catch (e) {
    next(e);
  }
});

router.patch('/api/loads/active/state', verifyToken, isDriver, async (req, res, next) => {
  try {
    const state = 'Arrived to delivery';
    await Load.findOneAndUpdate(
        {assigned_to: req.userId, status: 'ASSIGNED'},
        {state, status: 'SHIPPED'},
    );

    await Truck.findOneAndUpdate({assigned_to: req.userId}, {status: 'IS'});

    res.type('application/json').json({message: `Load state changed to ${state}`});

    logger.info({message: `Load state changed to ${state}`});
  } catch (e) {
    next(e);
  }
});

router.get('/api/loads/:id', verifyToken, checkRole, async (req, res, next) => {
  try {
    const filterField = req.userRole === 'SHIPPER' ? 'created_by' : 'assigned_to';
    const load = await Load
        .findOne({_id: req.params.id, [filterField]: req.userId})
        .select('-__v');
    if (!load) {
      return res.status(400).json({message: `No load with requested id ${req.params.id}`});
    }

    res.type('application/json').json(load);

    logger.info(load);
  } catch (e) {
    next(e);
  }
});

router.put('/api/loads/:id', verifyToken, isShipper, async (req, res, next) => {
  try {
    const filter = {_id: req.params.id, created_by: req.userId, status: 'NEW'};
    const load = await Load.findOne(filter);
    if (!load) {
      return res.status(400).json({message: `No loads with requested id ${req.params.id}`});
    }
    const newLoad = {...load._doc, ...req.body};

    await Load.findOneAndUpdate(filter, newLoad);

    res.type('application/json').json({message: 'Load details changed successfully'});

    logger.info({message: 'Load details changed successfully'});
  } catch (e) {
    next(e);
  }
});

router.delete('/api/loads/:id', verifyToken, isShipper, async (req, res, next) => {
  try {
    const load = await Load.findOneAndDelete({_id: req.params.id, created_by: req.userId, status: 'NEW'});
    if (!load) {
      return res.status(400).json({message: `No loads with requested id ${req.params.id}`});
    }

    res.type('application/json').json({message: 'Load deleted successfully'});

    logger.info({message: 'Load deleted successfully'});
  } catch (e) {
    next(e);
  }
});

router.post('/api/loads/:id/post', verifyToken, isShipper, async (req, res, next) => {
  try {
    const filterStatusNew = {_id: req.params.id, created_by: req.userId, status: 'NEW'};
    const load = await Load.findOne(filterStatusNew);
    if (!load) {
      return res.status(400).json({message: `No loads with requested id ${req.params.id}`});
    }

    await Load.findOneAndUpdate(filterStatusNew, {status: 'POSTED'});

    const truck = await Truck.findOneAndUpdate(
        {assigned_to: {$exists: true}, status: 'IS'},
        {status: 'OL'},
    );

    const filterStatusPosted = {_id: req.params.id, created_by: req.userId, status: 'POSTED'};
    if (!truck) {
      await Load.findOneAndUpdate(filterStatusPosted, {
        status: 'NEW',
        $push: {logs: {
          message: `Load assign failed, no trucks for now`,
          time: new Date().toLocaleString(),
        }},
      });
      return res.status(200).json({message: 'We do not have trucks for now'});
    }

    await Load.findOneAndUpdate(
        filterStatusPosted,
        {
          assigned_to: truck.assigned_to,
          status: 'ASSIGNED',
          state: 'En route to Pick Up',
          $push: {logs: {
            message: `Load assigned to driver with id ${truck.assigned_to}`,
            time: new Date().toLocaleString(),
          }},
        },
    );

    const resBody = {
      message: 'Load posted successfully',
      driver_found: true,
    };

    res.type('application/json').json(resBody);

    logger.info(resBody);
  } catch (e) {
    next(e);
  }
});

router.get('/api/loads/:id/shipping_info', verifyToken, isShipper, async (req, res, next) => {
  try {
    const load = await Load
        .findOne({_id: req.params.id, created_by: req.userId})
        .select('-__v');
    if (!load) {
      return res.status(400).json({message: `No load with requested id ${req.params.id}`});
    }

    const {assigned_to} = load;

    const truck = await Truck.findOne({assigned_to}).select('-__v');

    res.type('application/json').json({load, truck});

    logger.info(load);
  } catch (e) {
    next(e);
  }
});

module.exports = router;
