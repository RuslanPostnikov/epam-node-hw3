const Joi = require('joi');
const logger = require('../../logger');

const truckType = Joi.valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT');


const validateTruckType = (req, res, next) => {
  try {
    const type = req.body.type;
    const {error} = truckType.validate(type);
    if (error) {
      const message = error.details[0].message;
      logger.error({message});
      return res.status(400).json({message});
    }

    next();
  } catch (e) {
    console.log(e);
  }
};

module.exports = {validateTruckType};
