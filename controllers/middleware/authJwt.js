const jwt = require('jsonwebtoken');
const User = require('../../models/user.model');

verifyToken = (req, res, next) => {
  const token = req.headers['authorization'].split(' ')[1];
  if (!token) {
    return res.status(403).send({message: 'No token provided!'});
  }
  jwt.verify(token, process.env.SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).send({message: 'Unauthorized!'});
    }
    req.userId = decoded.id;
    next();
  });
};

checkRole = async (req, res, next) => {
  const user = await User.findById(req.userId);
  req.userRole = user.role;
  next();
};

isShipper = async (req, res, next) => {
  const user = await User.findById(req.userId);
  if (user.role !== 'SHIPPER') {
    return res.status(401).send({message: 'You need to be Shipper to access this data!'});
  }
  req.userRole = req.body;
  next();
};

isDriver = async (req, res, next) => {
  const user = await User.findById(req.userId);
  if (user.role !== 'DRIVER') {
    return res.status(401).send({message: 'You need to be Driver to access this data!'});
  }
  next();
};


module.exports = {verifyToken, checkRole, isShipper, isDriver};
