const User = require('../../models/user.model');

checkDuplicateEmail = (req, res, next) => {
  // Username
  User.findOne({
    email: req.body.email,
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({message: err});
      return;
    }
    if (user) {
      res.status(400).send({message: 'Failed! User is already in use!'});
      return;
    }
    next();
  });
};

module.exports = checkDuplicateEmail;
