const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
require('dotenv').config();

const errorMiddleware = require('./controllers/middleware/errorMiddleware');
const logger = require('./logger');
const authRoutes = require('./controllers/routes/auth');
const userRoutes = require('./controllers/routes/user');
const truckRoutes = require('./controllers/routes/truck');
const loadRoutes = require('./controllers/routes/load');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use(authRoutes);
app.use(userRoutes);
app.use(truckRoutes);
app.use(loadRoutes);

app.use(errorMiddleware);


const PORT = process.env.PORT || 8080;


async function start() {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(PORT, () => {
      logger.info(`Server is running on port ${PORT}`);
    });
  } catch (e) {
    logger.error(e);
  }
}

start();

